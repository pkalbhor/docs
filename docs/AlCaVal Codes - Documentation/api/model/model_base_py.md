# model_base_py

This module contains the `ModelBase` class.

## Class `ModelBase`

The `ModelBase` class inherits from the `PdmVModelBase` class in the `core_lib.model.model_base` module. It provides some convenience methods and has a smart setter mechanism.

### Attributes

The `ModelBase` class defines the following private attributes:

- `__cmssw_regex`: Regular expression to validate the CMSSW version format.
- `__dataset_regex`: Regular expression to validate the dataset name format.
- `__globaltag_regex`: Regular expression to validate the Global Tag format.
- `__relval_regex`: Regular expression to validate the RelVal format.
- `__ps_regex`: Regular expression to validate the Processing String format.
- `__sample_tag_regex`: Regular expression to validate the Sample Tag format.

### Methods

The `ModelBase` class defines the following public method:

- `default_lambda_checks`: Dictionary containing the default checks for different model parameters. Each key in the dictionary represents a parameter, and the associated value is a lambda function that performs the corresponding check. The checks include validation using regular expressions and numeric constraints.

#### Example Checks

- `batch_name`: Checks if the batch name matches a specific format.
- `cmssw_release`: Checks if the CMSSW version matches the expected format.
- `cpu_cores`: Checks if the number of CPU cores is between 1 and 8.
- `dataset`: Checks if the dataset name matches the expected format.
- `globaltag`: Checks if the Global Tag matches the expected format.
- `label`: Checks if the label matches a specific format.
- `matrix`: Checks if the matrix is one of the allowed values.
- `memory`: Checks if the memory value is between 0 and 32000.
- `processing_string`: Checks if the Processing String matches the expected format.
- `relval`: Checks if the RelVal matches the expected format.
- `sample_tag`: Checks if the Sample Tag matches the expected format.
- `scram_arch`: Checks if the Scram Arch matches a specific format.

### Usage

To use the `ModelBase` class, import it from the `core_lib.model.model_base` module and inherit from it in your model classes.

Please see the [source code](https://github.com/cms-AlCaDB/AlCaVal/blob/main/api/model/model_base.py) for the exact implementation.
