# ticket_py

# Class `dict_or_list`

This module contains the `dict_or_list` class which provides functionality to convert input string into a list or a dictionary.

## Method: `typecast(input_string)`

This method aims to typecast the `input_string` into a list or a dictionary based on its content.

### Parameters: 

- `input_string`: It is the input string that needs to be typecasted.

### Returns:

- The typecasted `input_string` as a list or a dictionary based on its content.

### Raises Exception:

- An exception is raised with the message 'Input is required to be list or dict in string format' if the `input_string` cannot be typecasted into a list or a dictionary.

The method follows the steps below:

1. If the `input_string` is already a list or a dictionary, it simply returns the `input_string`.

2. If the `input_string` is an empty string, it returns an empty list.

3. If the `input_string` can be evaluated as a dictionary, it returns the `input_string` evaluated as a dictionary.

4. If the `input_string` can be evaluated as a tuple, it returns the `input_string` evaluated as a list.

5. If the `input_string` can be evaluated as an integer, it returns a list containing the `input_string` evaluated as an integer.

6. If none of the above conditions are satisfied or an error occurs during evaluation, the method raises an exception with the message 'Input is required to be list or dict in string format'.

# Class `Ticket`

This class extends from `ModelBase` and allows to create multiple similar RelVals in the same campaign.

## Attributes

1. `_ModelBase__schema`: This dictionary attribute includes key-value pairs that define the structure of a `Ticket` object. Each key represents a property of the `Ticket` and the value represents the default value for that property.

2. `lambda_checks`: This dictionary includes key-value pairs where each key is an attribute name from the `_ModelBase__schema` dictionary, and the value is a lambda function which checks if the value of that attribute meets certain criteria.

## Detailed Description of `_ModelBase__schema`

The `_ModelBase__schema` includes the following keys:

1. `_id`: Database id (required by database)
2. `prepid`: PrepID
3. `batch_name`: Batch name
4. `cmssw_release`: CMSSW release
5. `jira_ticket`: Jira ticket
6. `title`: Title/Purpose of the validation
7. `cms_talk_link`: CMS-Talk link
8. `hlt_menu`: Custom HLT Menu
9. `hlt_gt`: HLT GT
10. `prompt_gt`: Prompt GT
11. `express_gt`: Express GT
12. `common_prompt_gt`: Common Prompt GT
... and so on.

## Detailed Description of `lambda_checks`

The `lambda_checks` includes the following keys:

1. `prepid`: Checks if the `prepid` matches a certain regex pattern.
2. `batch_name`: Checks if the `batch_name` matches the conditions specified in the `lambda_check` method of `ModelBase`.
3. `cmssw_release`: Checks if the `cmssw_release` matches the conditions specified in the `lambda_check` method of `ModelBase`.
4. `cpu_cores`: Checks if the `cpu_cores` matches the conditions specified in the `lambda_check` method of `ModelBase`.
5. `__created_relvals`: Checks if the `__created_relvals` matches the conditions specified in the `lambda_check` method of `ModelBase`.
6. `_gpu`: Checks multiple conditions related to `gpu` such as if `requires` is in `('forbidden', 'optional', 'required')`, if `cuda_capabilities` is a list, and if `gpu_memory` is not empty or greater than 0.
... and so on.

## Method `__init__`

The `__init__` method is the initializer (constructor) for the `Ticket` class. It takes two optional parameters:

1. `json_input` (default `None`): A dictionary containing key-value pairs that define a `Ticket` object. Each key should correspond to a key in the `_ModelBase__schema` dictionary.

2. `check_attributes` (default `True`): A boolean that determines whether the attributes should be checked against `lambda_checks` upon initialization.

### Detailed Description

- The `json_input` is first deep copied to prevent changes to the original dictionary that was passed in.
- Next, each `workflow_id` in the `workflow_ids` list is converted to a float.
- The `recycle_gs` attribute is retrieved from the dictionary. If it does not exist, `False` is used as the default value. The result is then converted to a boolean.
- If the `requires` attribute of `gpu` is neither `optional` nor `required`, the `gpu` dictionary in the `json_input` is replaced with the `gpu` dictionary from the `_ModelBase__schema` dictionary. Also, `requires` is set to `forbidden` and `gpu_steps` is set to an empty list.
- Finally, the `ModelBase`'s `__init__` method is called with `json_input` and `check_attributes` as arguments.

Please see the [source code](https://github.com/cms-AlCaDB/AlCaVal/blob/main/api/model/ticket.py) for the exact implementation.



