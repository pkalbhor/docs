# Capítulo 2

Este é o segundo capítulo en galego.

## Sección 1

Este é a primeira sección do segundo capítulo.

Esto é **texto en negriña**. Esto é *texto en cursiva*.

MkDocs proporciona [unha breve guía de como escribir en Markdown](https://www.mkdocs.org/user-guide/writing-your-docs/#writing-with-markdown){target=_blank}.

## Sección 2

Este é a segunda sección do segundo capítulo.

### Subsección 1

Este é a primeira subsección da segunda sección do segundo capítulo.

#### Subsubsection 1

Este é a primeira subsubsección da primeira subsección da segunda sección do segundo capítulo.

##### Level 5

Esto é outro nivel (5º) da estrucutra deste contido.

###### Level 6

Esto é outro nivel (6º) da estrucutra deste contido.
